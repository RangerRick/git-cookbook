# Frozen Buttermilk Biscuits

source: [It's Always Freezer Season](https://ac-restaurants.myshopify.com/products/its-always-freezer-season-cookbook) (p. 57, Kindle edition). Christensen, Ashley; Goalen, Kaitlyn.

Biscuits are one of those rare things that actually improve when frozen (pie dough and cookie dough also fall into this category).
It’s likely about the butter.

For this recipe, we use the grated frozen butter method.
This technique suspends little butter flakes throughout the dough, which rise and create flaky pastry during cooking.

Freezing the dough before baking ensures the fat and milk solids stay put (essential for creating a flaky result).

Biscuits bake quickly and can lean sweet or savory with ease.
Use them to top a blueberry cobbler, or treat your biscuit dough like gnocchi, slicing it into small pillows or dumplings.

## Ingredients

### Dough

* 2½ lbs self-rising flour (9 cups), plus more if needed
* 1 tbsp (18g) kosher salt
* 1 lb unsalted butter, frozen
* 4 cups whole buttermilk

### Self-Rising Flour Substitute

If you don't have self-rising flour, whisk these into the flour as well:

* 14tsp (43g) baking powder
* 4.5tsp (27g) kosher salt

## Directions

### Making the Biscuits

In a large bowl, stir together the flour and salt.

Using the large holes of a box grater, grate the frozen butter into the flour mixture, stirring and coating the butter with flour as you grate.
Once all of the butter is in, use a pastry blender to distribute the butter evenly in the flour.
It should have a pebbly texture.

Next, pour in the buttermilk and, using your hands, incorporate it with the flour mixture until a shaggy dough ball forms.

Turn the dough out onto a _well-floured_ work surface and, still using your hands, carefully pat it into a loaf shape, incorporating more flour as needed until the dough is not sticky and is easy to manipulate.
Gently pat the dough out into an even layer about ¾ inch thick.

> A note on technique: the "well-floured" is emphasized here because the best biscuits come from a rather wet dough, trapped inside a layer of dry flour.
> 
> I screwed this up the first time I made these biscuits; even though the ratios and butter distribution were perfect, my biscuits were a mess because they didn't have that "seal" of flour.
> You need it to hold the moisture in as they bake, and for them to crisp up nicely on the outside.
> True masters can fold and pat _after_ flouring to create flaky layers. I am not yet a true master. :)

Using a 2½-inch round biscuit cutter, and firmly pressing straight down (without twisting the cutter), punch out as many biscuits as possible.

Gather up the dough scraps and gently form into a ball.
Press the dough out into a ¾-inch-thick layer and repeat to punch out more biscuits until you have about 24 total.

### Freezing

Arrange the cut biscuit dough on a rimmed baking sheet and freeze for at least 4 hours or up to overnight for a formative freeze.
Transfer to a gallon-size zip-top plastic bag, label and date, and freeze for up to 4 months.

### Baking

Preheat the oven to 400°F.
Place the biscuits, with their edges touching, on a rimmed baking sheet and top each with a small pat of butter.
Bake for 10 to 13 minutes from room temperature or 14 to 16 minutes from frozen, until puffed and golden brown on top.

## Variants

### Biscuit Dumplings or “Gnocchi”

Instead of punching out the biscuit dough into rounds, cut it into strips about ¾ inch wide.
Then cut each strip crosswise into 1-inch-long pieces (they’ll look like little dough pillows).
Freeze as directed for the biscuits.

### Orange Biscuits

In a medium bowl, combine ½ cup granulated sugar and the finely grated zest of 2 oranges.
Use your fingers to rub the zest into the sugar until the sugar is damp like wet sand.

Mix the sugar with the flour and salt at the beginning of the recipe and proceed as written.

While the biscuits are in the oven, make a glaze:

In a medium saucepan over medium heat, whisk together 3 cups confectioners’ sugar, ½ cup fresh orange juice, ½ teaspoon kosher salt, and the finely grated zest of 2 oranges.

Bring the mixture to a simmer and let cook for about 6 minutes, until the mixture is smooth and the sugar has dissolved.
Remove from the heat and let cool for 10 minutes.

Whisk in 8 ounces softened cream cheese until the glaze is smooth.

Let the biscuits cool for 10 minutes after removing them from the oven.

Then drizzle some of the glaze over each biscuit before serving.
