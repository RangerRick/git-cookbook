# Sous Vide NC Pulled-Pork BBQ ([source](https://fundiegofamily.com/cooking/sous-vide-north-carolina-pulled-pork/))

<img src="./bbq.jpg" alt="the BBQ" width="33%" style="float:right">

I loosely based this on the recipe referenced in the source, but I have adjusted it to match how we like it.

Don't tell anyone, but honestly, I think this tastes better than "real" smoked NC pulled pork.
The process of reducing all the liquid and putting it back into the meat takes it to a whole new level.

## Ingredients

### BBQ

* 2.5 gallon Ziploc bag (or equivalent)
* 6-7 lbs bone-in pork shoulder
* 2 tbsp paprika
* 2 tbsp brown sugar
* 2 tbsp kosher salt
* 1 tbsp chili powder
* 1 tbsp cayenne pepper
* 1 tbsp cumin
* 1 tbsp fresh ground black pepper
* 1-2 tbsp liquid smoke

### Sauce (from [here](https://fundiegofamily.com/cooking/cole-cooks-north-carolina-bbq-vinegar-sauce-recipe/))

* 2 cups apple cider vinegar
* 2 tbsp ketchup
* 2 tbsp hot sauce (Texas Pete, Frank's, etc.)
* 2 tbsp brown sugar
* 1 tbsp crushed red pepper flakes
* 2 tsp salt
* 2 tsp black pepper

## Equipment

<img src="./anova-cooker.jpg" alt="my Anova Cooker setup" width="33%" style="float:right">

You will want a sous vide setup that will let you run for a day without having to replenish the water.
This means a sealed container, ideally with an insulating sleeve.

I have one of the early Anova Precision Cooker devices.
The particular combination of insulating sleeve and cover that I got doesn't seem to exist anymore, but these should give you basically the same setup:

* [Rubbermaid 12-quart food storage container](https://www.amazon.com/Rubbermaid-Commercial-Container-12-Quart-FG631200CLR/dp/B000R8JOUC/ref=sr_1_2?crid=2DDGGPGOHFCEZ&keywords=rubbermaid+12+quart+container&qid=1687105912&sr=8-2)
* [EVERIE sous vide sleeve](https://www.amazon.com/EVERIE-Container-Neoprene-Rubbermaid-Electricity/dp/B07788BYTT/ref=sr_1_3?crid=1D871RITSXLEQ&sr=8-3)
* [EVERIE hinged lid](https://www.amazon.com/EVERIE-Collapsible-Container-Compatible-Rubbermaid/dp/B071L6PRY8/ref=sr_1_8?crid=1D871RITSXLEQ&qid=1687106123&sr=8-8)

## Preparation

1. In a small bowl, combine the dry ingredients and the liquid smoke.
2. Place the pork shoulder in the Ziploc bag.

   It could probably fit in something smaller, but I like having enough room to spread the dry rub easily.
3. Apply the rub evenly all over the meat.
4. Place the bag into the sous vide container with the top pointing up, and fill it up.
   Make sure the water pushes all of the air out of the bag, then zip the bag closed.

## Cooking

1. Cook at 170℉ for 24 hours.

   With my setup I can just let it run without having to do anything else, but you will want to check occasionally to make sure you're not getting any evaporation and the meat stays immersed.
2. When complete, move the meat to a large baking dish or similar.
3. Pour the liquid from the bag through a strainer into a pot and throw away the solids.
4. Bring the liquid to a boil on the stovetop at medium heat, then reduce the temperature until it settles into a mild simmer.

   You will want to occasionally check on it to skim the fat or any bubbles that form off the top.
5. While it's reducing, shred the pork, removing the bone and any large chunks of fat.

   I use [these OXO "shredding claws"](https://www.amazon.com/OXO-11164700-Grips-Shredding-Handles/dp/B01434TXH4/ref=sr_1_3?crid=1K1WF2BHYLLDQ&keywords=oxo+claws&qid=1687106705&sprefix=oxo+claws%2Caps%2C103&sr=8-3) and they do the job quite nicely, but you can get away with using forks, it's just a bit more work.
   The meat is so tender you could almost just do it with your hands.
6. When the juice has reduced to less than a cup or so, pour it over the meat and mix well to combine.

   It should reduce by 80% or so and thicken up quite a bit, but not so much that it's syrupy.
7. Clean out the pot, and then add the sauce ingredients, whisking to combine.
8. Bring to a boil, then reduce the heat and simmer for 15 minutes.
9. Let cool and store in a jar or squeeze bottle.

## Tips

1. I let the juices from the BBQ cool and then stick it in the fridge overnight rather than applying it to the meat right away.
   That way if there's any more fat left, I can scrape it off the top, and then reheat it in the microwave enough to pour it over the meat.
2. We usually keep enough for a couple of days' worth of meals in the fridge, and break the rest of the BBQ into portions and freeze it.

   It freezes _really_ well, a 7 pound shoulder gives the two of us enough servings to have it quite a few times before I need to cook another shoulder.
3. We use [Stasher silicone bags](https://www.stasherbag.com/) for freezer storage.
   They're nice because they're reusable, and they can go straight from the freezer to the microwave if you're in a hurry, although we usually just take a bag out of the freezer and stick it in the fridge overnight to defrost.
4. You can skip the simmer step with the sauce if you want.
   It will combine better when heated, but we usually don't bother and just shake it real well. 😉
